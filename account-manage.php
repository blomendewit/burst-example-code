<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>
  
  <div class="wrapper">
   <h1 class="header">Abonnement(en) beheren</h1>   

   <div class="addchild__form">
    <p>Hier kunt u alle betalingen van de Stack gebruikers beheren. Het is mogelijlk om een abonnement stop te zetten of te verlengen. Zodra u de betaling stop zet kan de Stack gebruiker het abonnement gebruiken tot de eind datum. Er worden geen betalingen meer afgeschreven.</p>
    
    <div class="changechild__wrapper">
      <div class="changechild--whole">
        <div class="changechild__row">
          <img class="child__image" src="/src/img/johan-small.png" alt="">
          <span class="child__name">Johan Bouwer</span>
          <span class="child__expired">Loopt af op 25-08-2016</span>              
          <a href="#" class="child__expand">arrow</a>            
        </div>
        <div class="changechild__buttons">
          <a href="#" class="btn btn--dark">Abonnement verlengen</a>
          <a href="#" class="btn">Abonnement stopzetten</a>
        </div>
      </div>

      <div class="changechild--whole">
        <div class="changechild__row">
          <img class="child__image" src="src/img/johan-small.png" alt="">
          <span class="child__name">Saskia Bouwer</span>
          <span class="child__expired">Loopt af op 25-08-2016</span>  
          <a href="#" class="child__expand">arrow</a>            
        </div>
        <div class="changechild__buttons">
          <a href="#" class="btn btn--dark">Abonnement verlengen</a>
          <a href="#" class="btn">Abonnement stopzetten</a>
        </div>
      </div>

      <div class="changechild--whole">
        <div class="changechild__row">
          <img class="child__image" src="src/img/johan-small.png" alt="">
          <span class="child__name">Teske Bouwer</span>
          <span class="child__expired">Loopt af op 25-08-2016</span>              
          <a href="#" class="child__expand">arrow</a>            
        </div>
        <div class="changechild__buttons">
          <a href="#" class="btn btn--dark">Abonnement verlengen</a>
          <a href="#" class="btn">Abonnement stopzetten</a>
        </div>
      </div>

      <div class="changechild--whole changechild--whole--inactive">
        <div class="changechild__row">
          <img class="child__image" src="src/img/johan-small.png" alt="">
          <span class="child__name">Joost Bouwer</span>
          <span class="child__expired">Inactief</span>              
          <a href="#" class="child__expand">arrow</a>            
        </div>
        <div class="changechild__buttons">
          <a href="#" class="btn btn--dark">Abonnement verlengen</a>
        </div>
      </div>
    </div>
  </div>
</div>

</div> 


<?php include('sections/footer.php'); ?>