<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title>Developed by Tom Harms Development</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0, minimal-ui">

  <link href="/src/css/style.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js"></script>

  <script>
  WebFont.load({
    custom: {
      families: ['opensans'],
      urls: ['src/fonts/opensans/opensans.css']
    }
  });
  </script>

</head>
<body>
  <div class="site-wrap">
    <div class="site-canvas site-canvas--inlog">
      <header>
        <div class="logo">
          <img src="src/img/logo.svg" alt="">
        </div>
      </header>

      <div class="center__block">
        <h1>Inloggen</h1>
        <form class="default-form" action="#">
          <div class="form__row">
            <input type="text" name="firstname" placeholder="Voornaam">
          </div>

          <div class="form__row">
            <input type="text" name="mail" placeholder="E-mail adres">
            <a href="#" class="forget__password">Wachtwoord vergeten</a>
          </div>

          <div class="form__row--buttons">
            <a href="/dashboard-johan.php" class="btn--large btn--dark pull-left">Inloggen</a>
            <a href="/register.php" class="btn--large pull-right">Account aanmaken</a>
          </div>

        </form>
      </div>



      <?php include('sections/footer.php'); ?>