<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>

  <div class="wrapper">
    <h1 class="header">Berichten</h1>

    <div class="message__nav">
      <div class="message__nav--left">
        <a href="#" class="strong">Inbox</a>
        <a href="#">Verzonden</a>
        <a href="#">Bewaard</a>
        <a href="#">Verwijderd</a>
      </div>

      <div class="message__nav--right">
        <a href="#" class="btn">Verwijderen</a>
        <a href="#" class="btn--more">Meer</a>
        <ul class="messages__list">
          <li><a href="#" class="btn--dropdown">Mark. ongelezen</a></li>
          <li><a href="#" class="btn--dropdown">Johan</a></li>
          <li><a href="#" class="btn--dropdown">Saskia</a></li>
        </ul>
      </div>
    </div>

    <div class="inbox">

      <div class="message__from">
        <img class="from__image" src="src/img/judith-small.png" alt="">

        <div class="from__info">
          <span class="info__name">Judith Visser</span>
          <span class="info__group">Groep 6</span>
        </div>

        <div class="from__date">
          <span>9 Nov 2015</span>
        </div>
      </div>

      <div class="message__content">
        <h3 class="message__title">Rekentoets slecht gemaakt</h3>
        <a href="#" class="btn__reply"></a>

        <p>
          Beste meneer Bouwer,
        </br></br>
        Uw zoon heeft de laatste rekentoets onder niveau gehaald. Nu ik kijk op zijn Stack account, zie ik dat hij hier 
        ook niet heel goed presteert. Zou u hem een tip kunnen geven om eerst verder te gaan met het opbouwen van 
        de kapperszaak? Deze sommen sluiten goed aan op het onderwerp waar hij nu veel moeite mee heeft. 
      </br></br>
      M.v.g.
    </br>
    Judith Visser
  </p>
</div>
</div>
</div> 


<?php include('sections/footer.php'); ?>