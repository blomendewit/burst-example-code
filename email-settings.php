<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>
  
  <div class="wrapper">
    <h1 class="header">E-mail notificaties</h1>   
    <p>Stel hier uw e-mail notificaties in. Deze e-mail notificaties hangen samen met de gebruikers die u toe heeft gevoegd aan de Stack Stats applicatie. Met deze notificaties kunt u de voortgang van de gebruikers mee in de gaten houden.</p>

    <div class="switches">
      <div class="switch__row">
        <span class="switch__name">Zodra badge is gewonnen</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-1" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-1"></label>
          <span>Aan</span>
        </div>          
      </div>

      <div class="switch__row">
        <span class="switch__name">Zodra huis is opgebouwd</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-2" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-2"></label>
          <span>Aan</span>
        </div>          
      </div>

      <div class="switch__row">
        <span class="switch__name">Zodra huis af is</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-3" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-3"></label>
          <span>Aan</span>
        </div>          
      </div>

      <div class="switch__row">
        <span class="switch__name">Zodra leraar iets stuurt</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-4" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-4"></label>
          <span>Aan</span>
        </div>          
      </div>

      <div class="switch__row">
        <span class="switch__name">Zodra er 5 dagen niet is ingelogd</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-5" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-5"></label>
          <span>Aan</span>
        </div>          
      </div>

      <div class="switch__row">
        <span class="switch__name">Zodra er een nieuw level is behaald</span>
        <div class="switch pull-right">
          <span>Uit</span>
          <input id="cmn-toggle-6" class="cmn-toggle cmn-toggle-round" type="checkbox">
          <label for="cmn-toggle-6"></label>
          <span>Aan</span>
        </div>          
      </div> 
    </div>       
  </div>

</div> 


<?php include('sections/footer.php'); ?>