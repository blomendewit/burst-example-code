<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>

  <div class="wrapper">
    <h1 class="header">Berichten</h1>

    <div class="message__nav">
      <div class="message__nav--left">
        <a href="#" class="strong">Inbox</a>
        <a href="#">Verzonden</a>
        <a href="#">Bewaard</a>
        <a href="#">Verwijderd</a>
      </div>

      <div class="message__nav--right">
        <a href="#" class="btn">Verwijderen</a>
        <a href="#" class="btn--more">Meer</a>
        <ul class="messages__list">
          <li><a href="#" class="btn--dropdown">Mark. ongelezen</a></li>
          <li><a href="#" class="btn--dropdown">Johan</a></li>
          <li><a href="#" class="btn--dropdown">Saskia</a></li>
        </ul>
      </div>
    </div>

    <div class="inbox">
      <div class="inbox__header">
        <span class="column1"></span>
        <span class="column2">Afzender</span>
        <span class="column3">Bericht</span>
        <span class="column4"></span>
      </div>
      
      <a href="messages-detail.php" class="message__link">
        <div class="message__row new">
          <div class="column column1 column1__select">
            <input type="checkbox" id="message1" />
            <label for="message1"></label>
          </div>
          <span class="column column2">Judith Visser</span>
          <span class="column column3">Rekentoets slecht gemaakt</span>
          <span class="column column4">Beste meneer Bouwer, uw zoon</span>
          <span class="column column5">9 nov</span>
        </div>
      </a>

      <a href="messages-detail.php" class="message__link">
        <div class="message__row">
          <div class="column column1 column1__select">
            <input type="checkbox" id="message2" />
            <label for="message2"></label>
          </div>
          <span class="column column2">Judith Visser</span>
          <span class="column column3">Rekentoets slecht gemaakt</span>
          <span class="column column4">Beste meneer Bouwer, uw zoon</span>
          <span class="column column5">9 nov</span>
        </div>
      </a>

      <a href="messages-detail.php" class="message__link">
        <div class="message__row">
          <div class="column column1 column1__select">
            <input type="checkbox" id="message3" />
            <label for="message3"></label>
          </div>
          <span class="column column2">Judith Visser</span>
          <span class="column column3">Rekentoets slecht gemaakt</span>
          <span class="column column4">Beste meneer Bouwer, uw zoon</span>
          <span class="column column5">9 nov</span>
        </div>
      </a>

      <a href="messages-detail.php" class="message__link">
        <div class="message__row">
          <div class="column column1 column1__select">
            <input type="checkbox" id="message4" />
            <label for="message4"></label>
          </div>
          <span class="column column2">Judith Visser</span>
          <span class="column column3">Rekentoets slecht gemaakt</span>
          <span class="column column4">Beste meneer Bouwer, uw zoon</span>
          <span class="column column5">9 nov</span>
        </div>
      </a>
    </div>
  </div>

</div> 


<?php include('sections/footer.php'); ?>