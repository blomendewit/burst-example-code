<?php include('sections/header.php'); ?>

<div class="popup">
  <a href="#" class="close"></a>
  <h1>Gebruiker toegevoegd</h1>
  <p>U heeft met succes een account aangemaakt voor Teske. Bij uw berichten staan de gegevens van de neiuwe gebruiker klaar.</p>
  <a href="#" class="btn--large btn--dark btn--popup">Oke</a>
</div>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>
  
  <div class="wrapper">
    <h1 class="header">Gebruiker toevoegen</h1>   
    <p>Hier kunt u alle betalingen van de Stack gebruikers beheren. Het is mogelijlk om een abonnement stop te zetten of te verlengen. Zodra u de betaling stop zet kan de Stack gebruiker het abonnement gebruiken tot de eind datum. Er worden geen betalingen meer afgeschreven.</p>

    <div class="addchild__form">
      <h2>Persoonlijke gegevens</h2>
      <form class="default-form" action="#">
        <div class="form__row">
          <input class="pull-left" type="text" name="firstname" placeholder="Voornaam">
          <input class="pull-right" type="text" name="lastname" placeholder="Achternaam">
        </div>

        <div class="form__row">
          <input class="pull-left" type="text" name="mail" placeholder="E-mail adres">
          <input class="pull-right" type="password" name="pass" placeholder="Wachtwoord">
        </div>

      </form>
    </div>

    <div class="addchild__form">
      <h2>Vakken</h2>
      <p>Welke resultaten van welke vakken wilt u bekijken in de Stack Stats applicatie?</p>
      <form class="default-form no-padding" action="#">
        <div class="form__row">
          <input type="checkbox" id="math"/>
          <label for="math">Rekenen</label>

          <input type="checkbox" id="topo"/>
          <label for="topo">Topografie</label>

          <input type="checkbox" id="history"/>
          <label for="history">Geschiedenis</label>
        </div>

        <div class="form__row">
          <input type="checkbox" id="reading"/>
          <label for="reading">Begrijpend lezen</label>

          <input type="checkbox" id="dictee"/>
          <label for="dictee">Dictee</label>

          <input type="checkbox" id="geographic"/>
          <label for="geographic">Aardijkskunde</label>
        </div>
      </form>
    </div>

    <div class="addchild__form">
      <h2>Abonnement</h2>
      <p>Hier kunt u een abonnementsvorm kiezen voor de nieuwe Stack gebruiker. Zodra u de betaling stop zet bij uw instellingen (account of betalingen) kan de Stack gebruiker het abonnement gebruiken tot de eind datum. Er worden geen betalingen meer afgeschreven.</p>
      <form class="default-form" action="#">
        <div class="form__row">
          <input type="radio" id="short" name="term" />
          <label for="short"><span></span>3 maanden (&#8364;9,99)</label>

          <input type="radio" id="medium" name="term" />
          <label for="medium"><span></span>6 maanden (&#8364;16,99)</label>

          <input type="radio" id="long" name="term" />
          <label for="long"><span></span>1 jaar (&#8364;29,99)</label>
        </div>
      </form>
    </div>

    <div class="addchild__form">
      <h2>Betaling</h2>
      <p>Kies hier de wijze van betaling. Zodra u op gebruiker toevoegen klikt wordt u doorverwezen naar uw bank en kunt u uw betaling voltooien.</p>
      <form class="default-form" action="#">
        <div class="form__row form__row--payment">
          <input type="radio" id="ideal" name="payment" />
          <label for="ideal"><span></span>Ideal</label>
          <span>Online betalen met je Nederlandse bank</span>

          <div class="payment__dropdown">
            <span>Kies je bank</span>
            <div class="contact__select">
              <select onchange="clickroute()">
                <option selected value="52.09074, 5.12142">Ing</option>
                <option value="52.52001, 13.40495">Rabobank</option>
                <option value="50.85034, 4.35171">Abn amro</option>
              </select>
            </div>
          </div>
        </div>

        <div class="form__row form__row--payment">
          <input type="radio" id="creditcard" name="payment" />
          <label for="creditcard"><span></span>Creditcard</label>
          <span>Snel en eenvoudig betalen</span>

          <div class="payment__dropdown">
            <img src="src/img/visa.svg" alt="">
            <img src="src/img/mastercard.svg" alt="">
            <img src="src/img/amex.svg" alt="">
            <a href="#" class="btn btn--dark">Kaart toevoegen</a>
          </div>
        </div>

        <div class="form__row form__row--payment">
          <input type="radio" id="paypal" name="payment" />
          <label for="paypal"><span></span>PayPal</label>
          <span>Veilig online betalen</span>
        </div>
      </form>
    </div>
    <a class="btn--large btn--dark" onclick="saveChild()">Gebruiker toevoegen</a>
    <a href="#" class="btn--large">Annuleren</a>        
  </div>

</div> 


<?php include('sections/footer.php'); ?>