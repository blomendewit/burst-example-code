<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>

  <div class="dashboard">
    <h1>Dashboard Saskia</h1>

    <div class="block__content__row">
      <div class="block__content--quarter">
        <img src="src/img/tijdsbesteding.svg" alt="">
      </div>

      <div class="block__content--half">
        <img src="src/img/sommen.svg" alt="">
      </div>   

      <div class="block__content--quarter">
        <img src="src/img/sessies.svg" alt="">
      </div>

    </div>

    <div class="block__content__row">

      <div class="block__content--half">
        <img src="src/img/moment.svg" alt="">
      </div>  

      <div class="block__content--quarter">
        <img src="src/img/duur.svg" alt="">
      </div>

      <div class="block__content--quarter">
        <img src="src/img/niveau.svg" alt="">
      </div>  
    </div>

    <div class="block__content__row">
      <div class="block__content--threequarter">
        <img src="src/img/voortgang.svg" alt="">
      </div>

      <div class="block__content--quarter">
        <img src="src/img/besteding.svg" alt="">
      </div>
    </div>

    <div class="block__content__row">
      <div class="block__content--whole">
        <img src="src/img/badges.svg" alt="">
      </div>
    </div>

    <div class="block__content__row">
      <div class="block__content--threequarter">
        <img src="src/img/behalen.svg" alt="">
      </div>

      <div class="block__content--quarter">
        <img src="src/img/achievements.svg" alt="">
      </div>
    </div>
  </div>
</div>

<?php include('sections/footer.php'); ?>