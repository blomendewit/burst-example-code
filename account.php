<?php include('sections/header.php'); ?>

<div class="container show">

  <?php include('sections/navigation-top.php'); ?>
  
  <div class="wrapper">
   <h1 class="header">Account beheren</h1>   

   <p>Wijzig hier gegevens voor uw account. Tevens kunt u hier uw account verwijderen.</p>

   <div class="addchild__form">

    <form class="default-form" action="#">
      <div class="form__row">
        <input class="pull-left" type="text" name="firstname" placeholder="Erik">
        <input class="pull-right" type="password" name="pass" placeholder="Oud wachtwoord">
      </div>

      <div class="form__row">
        <input class="pull-left" type="text" name="lastname" placeholder="Bouwer">
        <input disabled class="pull-right" type="password" name="pass" placeholder="Nieuw wachtwoord">
      </div>

      <div class="form__row">
        <input class="pull-left" type="text" name="mail" placeholder="erikbouwer@gmail.com">
        <input disabled class="pull-right" type="password" name="pass" placeholder="Herhaal nieuw wachtwoord">
      </div>

    </form>

    <a href="#" class="btn--large btn--dark">Wijzigingen opslaan</a>
    <a href="#" class="btn--large">Account verwijderen</a>
  </div>
</div>

<div class="wrapper">
  <h1 class="header">STACK GEBRUIKERS</h1>   

  <p>Wijzig hier gegevens voor de stack gebruikers. Tevens kunt u hier de accounts verwijderen.</p>
  
  <div class="changechild__wrapper">
    <div class="changechild">
      <img class="child__image" src="src/img/johan-small.png" alt="">
      <span class="child__name">Johan Bouwer</span>
      <span class="child__group">Groep 6</span>
      <span class="child__expired">Loopt af op 25-08-2016</span>
      <a href="#" class="btn btn--dark">Wijzig account</a>
    </div>

    <div class="changechild">
      <img class="child__image" src="src/img/johan-small.png" alt="">
      <span class="child__name">Johan Bouwer</span>
      <span class="child__group">Groep 6</span>
      <span class="child__expired">Loopt af op 25-08-2016</span>
      <a href="#" class="btn btn--dark">Wijzig account</a>
    </div>

    <div class="changechild">
      <img class="child__image" src="src/img/johan-small.png" alt="">
      <span class="child__name">Johan Bouwer</span>
      <span class="child__group">Groep 6</span>
      <span class="child__expired">Loopt af op 25-08-2016</span>
      <a href="#" class="btn btn--dark">Wijzig account</a>
    </div>

    <a href="/addchild.php">
      <div class="changechild changechild--add">
        <span>Nieuwe gebruiker toevoegen</span>
      </div>
    </a>

  </div>
</div>

</div> 


<?php include('sections/footer.php'); ?>