<!doctype html>
<html class="no-js">
<head>
  <meta charset="utf-8">
  <title>Stack stats - Developed by Flamingo</title>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0, minimal-ui">

  <link href="src/css/style.css" rel="stylesheet">
  <script src="//ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js"></script>

  <script>
  WebFont.load({
    custom: {
      families: ['opensans'],
      urls: ['src/fonts/opensans/opensans.css']
    }
  });
  </script>

</head>
<body>
  <div class="site-wrap">
    <div class="site-canvas">
      <header>
      </header>

      <div class="navigation">
        <nav class="navigation__side">

          <div class="navigation__logo">
            <img src="src/img/logo.svg" alt="">
          </div>
          <ul>
            <a href="dashboard-johan.php"><li class="child">
              <img src="src/img/johan-small.png" alt="">
              <span>Johan</span>
            </li></a>
            
            <a href="dashboard-saskia.php"><li class="child">
              <img src="src/img/saskia-small.png" alt="">
              <span>Saskia</span>
            </li></a>

            <a href="addchild.php"><li class="child child--add">
              <span>Toevoegen</span>
            </li></a>
          </ul>

          <ul class="navigation__bottom">

            <a href="messages.php"><li class="child child--image child--message">
              <span>Berichten</span>
            </li></a>

            <a href="account.php">
              <li class="child child--image child--settings">
                <span>Instellingen</span>
              </li>
            </a>
          </ul>
        </nav>
      </div>
      
      <ul class="navigation__bottom__submenu">
        <li><a href="account.php">Account</a></li>
        <li><a href="account-manage.php">Abonnement(en)</a></li>
        <li><a href="email-settings.php">Notificaties</a></li>
      </ul>