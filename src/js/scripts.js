var paymentClass;

$( document ).ready(function() {

 document.querySelector( ".nav-toggle" )
  .addEventListener( "click", function() {
    this.classList.toggle( "active" );
  });

  $('.navigation__side a').each(function(index) {
    console.log(this.href.trim());
    if(this.href.trim() == window.location) {
      $(this).children().addClass("child--active");
      $(this).addClass("child--border");
    }
  });

  $('.navigation__bottom__submenu li a').each(function(index) {
    console.log(this.href.trim());
    if(this.href.trim() == window.location) {
      $(this).addClass("selected");
      $( '.child--settings' ).addClass( "child--active" );
      $( '.child--settings' ).parent().addClass( "child--border" );
    }
  });
});

  $('.nav-toggle').click(function() {
    // Calling a function in case you want to expand upon this.
    toggleNav();
  });

  $('.child__expand').click(function(event) {
    // Calling a function in case you want to expand upon this.
    event.preventDefault();
    $(this).parent().parent().find(".changechild__buttons").toggleClass('changechild__colapse');
    $(this).toggleClass('changechild__colapse');
  });

  $('.btn--more').click(function() {
    // Calling a function in case you want to expand upon this.
    toggleDropdown();
  });

  $('#ideal, #creditcard, #paypal').click(function() {
    paymentClass = $(this).parent();
    togglePayment();
  });

  $('.close, .btn--popup').click(function(event) {
    event.preventDefault();
    $( '.site-canvas' ).toggleClass( 'overlay' );
    $( '.popup' ).toggleClass( 'show' );
  });

  $( '.child--settings' ).click(function(event) {
    event.preventDefault();
    $( '.navigation__bottom__submenu' ).toggleClass( 'hover' );
  });

  $( '.navigation__bottom__submenu' ).click(function(event) {
    $( '.navigation__bottom__submenu' ).toggleClass( 'hover' );
  });


function toggleNav() {
  console.log("toglled");

  if ($('.navigation').hasClass('show')) {
      // Do things on Nav Close
      $('.navigation').removeClass('show');
      $('.navigation__bottom__submenu').removeClass('show');
      $('.container').addClass('show');
      $('.navigation__top__right').removeClass('show');
  } else {
      // Do things on Nav Open
      $('.navigation').addClass('show');
      $('.navigation__bottom__submenu').addClass('show');
      $('.container').removeClass('show');
      $('.navigation__top__right').addClass('show');
  }
}

function toggleDropdown() {
  $('.messages__list').toggleClass("show");
  $('.btn--more').toggleClass("show");
}

function togglePayment() {
  if ($('.form__row--payment').hasClass('show')){
    $('.form__row--payment').removeClass('show');
    paymentClass.addClass('show');
    console.log(paymentClass);
  } else {
    paymentClass.addClass('show');
    console.log(paymentClass);
  }
}

function saveChild() {
  $( '.site-canvas' ).toggleClass( 'overlay' );
  $( '.popup' ).toggleClass( 'show' );
}